
# What is
This is an example project of usage ["git hooks"](https://git-scm.com/docs/githooks) of the "pre-commit" and "commit-msg".
It's a better practice for clean code, clear commits and therefore easier support of project maintenance.

# Usage
## Initial stage
    1. Clone repo: `git clone https://bitbucket.org/E1ectr0Nick/hook-test.git`
    2. Move into project: `cd hook-test`
    3. And run `npm install` or `yarn`.
__Notice! You must run the last step before committing (for install hooks in ".git/hooks/..."), else do you risk skip _BAD_ commits__

# Work
change or add your some files and commit

